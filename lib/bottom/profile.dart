import 'dart:html';
import 'dart:math';

import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(padding: const EdgeInsets.all(8), children: <Widget>[
      Container(
        child: CircleAvatar(
          radius: 45,
          backgroundColor: Colors.grey,
          child: CircleAvatar(
            radius: 45,
            backgroundImage: Image.network(
                    'https://i.seadn.io/gae/U_tMhcTTJmyztelSFm6IlmUqJN__hMqWlLUQmp4MOWlM2JLYJNYnvVLTh493Un1Y73tVbW6jv_opFbHFME39smu5mtEjE21eSHi6lDc?auto=format&w=512')
                .image,
          ),
        ),
      ),
      Container(
        child: ListTile(
          title: Text("แรบบิท ซุปเปอร์คูล", textAlign: TextAlign.center,style: TextStyle(fontSize: 18)),
          subtitle: Text("Rabbit Supercool", textAlign: TextAlign.center,style: TextStyle(fontSize: 16)),
        ),
      ),
      Container(
          // alignment: Alignment.centerLeft,
          margin: EdgeInsets.all(10),
          height: 150,
          width:double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
             borderRadius: BorderRadius.circular(30), //border corner radius
          ),
          child:Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text("ข้อมูลส่วนตัว:",style: TextStyle(fontSize: 16)),
            Text("    ชื่อ (ไทย): นายแรบบิท ซุปเปอร์คูล",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    ชื่อ (อังกฤษ): Rabbit Supercool",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    เลขบัตรประชาชน: 1234567890123",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    วันเกิด: 22 เม.ย. 2000",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    เพศ: ชาย",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    สัญชาติ: ไทย",style: TextStyle(fontSize: 14,color: Colors.grey)),
          ],)
       ),
       Container(
          margin: EdgeInsets.all(10),
          height: 150,
          width:double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
             borderRadius: BorderRadius.circular(30), //border corner radius
          ),
          child:Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text("ข้อมูลการศึกษา:",style: TextStyle(fontSize: 16)),
            Text("    รหัสนิสิต: 69160999",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    คณะ: วิทยาการสารสนเทศ",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    วิทยาเขต: บางแสน",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    สาขา: วิทยาการคอมพิวเตอร์",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    ระดับการศึกษา: ปริญญาตรี",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    ชั้นปี: 3",style: TextStyle(fontSize: 14,color: Colors.grey)),
          ],)
       ),
       Container(
          margin: EdgeInsets.all(10),
          height: 100,
          width:double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
             borderRadius: BorderRadius.circular(30), //border corner radius
          ),
          child:Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text("ผลการศึกษา:",style: TextStyle(fontSize: 16)),
            Text("    หน่วยกิตที่ลง: 100",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    หน่วยกิตที่ผ่าน: 100",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    คะแนนเฉลี่ยสะสม: 4.00",style: TextStyle(fontSize: 14,color: Colors.grey)),
          ],)
       ),
       Container(
          margin: EdgeInsets.all(10),
          height: 100,
          width:double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
             borderRadius: BorderRadius.circular(30), //border corner radius
          ),
          child:Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
            Text("ข้อมูลติดต่อ:",style: TextStyle(fontSize: 16)),
            Text("    Email: 69160999@go.buu.ac.th",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    ที่อยู่: 69/1 ม.6 ต.แสนสุข อ.เมือง จ.ชลบุรี",style: TextStyle(fontSize: 14,color: Colors.grey)),
            Text("    เบอร์โทร: 0969160999",style: TextStyle(fontSize: 14,color: Colors.grey)),
          ],)
       ),
    ]);
  }
}
