import 'package:flutter/material.dart';
import '../login.dart';
import '../setting/social.dart';
import '../setting/safety.dart';
import '../setting/language.dart';
import '../setting/register.dart';
import '../setting/warn.dart';

class Setting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Warn()),);}, 
        title: Text("การตั้งค่าการแจ้งเตือน"), leading: Icon(Icons.add_alert), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Register()),);}, 
        title: Text("การลงทะเบียนรับข่าวสาร"), leading: Icon(Icons.email), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Socail()),);}, 
        title: Text("เชื่อมต่อบัญชี"), leading: Icon(Icons.insert_link), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Safety()),);}, 
        title: Text("ความปลอดภัย"), leading: Icon(Icons.security), trailing: Icon(Icons.chevron_right)),
        // ListTile(onTap: () {}, title: Text("ธีม"), leading: Icon(Icons.swap_horiz), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Language()),);},
         title: Text("ภาษา"), leading: Icon(Icons.g_translate), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Login()),);},
         title: Text("Logout",style: TextStyle(color: Colors.red),), leading: Icon(Icons.logout,color: Colors.red,), trailing: Icon(Icons.chevron_right,color: Colors.red,)
  ),
      ],
    );
  }
}