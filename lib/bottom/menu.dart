import 'package:flutter/material.dart';
// import 'package:anim_search_bar/anim_search_bar.dart';
import '../menu/check_end.dart';
import '../menu/history.dart';
import '../menu/list_name.dart';
import '../menu/regis.dart';
import '../menu/request.dart';
import '../menu/results.dart';
import '../menu/table.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State {
  TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(padding: const EdgeInsets.only(top: 10), children: <Widget>[
      // AnimSearchBar(
      //   width: MediaQuery.of(context).size.width,
      //   textController: textController,
      //   onSuffixTap: () {
      //     setState(() {
      //       textController.clear();
      //     });
      //   },
      //   onSubmitted: (String) {},
      // ),
      Container(
        width: double.infinity,
        height: 40,
        child: const Center(
          child: TextField(
            decoration: InputDecoration(
              hintText: 'Search',
              prefixIcon: Icon(Icons.search),
            ),
          ),
        ),
      ),
      Padding(padding: EdgeInsets.only(top: 30)),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.contact_mail,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Regis()),
                    );
                  },
                ),
              ),
              Text("ลงทะเบียน"),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.grid_on,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TableS()),
                    );
                  },
                ),
              ),
              Text("ตารางเรียน"),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.business_center,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Results()),
                    );
                  },
                ),
              ),
              Text("ผลการศึกษา"),
            ],
          ),
        ],
      ),
      Padding(padding: EdgeInsets.only(top: 70)),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.location_city,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Check_End()),
                    );
                  },
                ),
              ),
              Text("ตรวจสอบจบ"),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.playlist_add_check,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => List_Name()),
                    );
                  },
                ),
              ),
              Text("รายชื่อนิสิต"),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.send,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Request()),
                    );
                  },
                ),
              ),
              Text("ยื่นคำร้อง"),
            ],
          ),
        ],
      ),
      Padding(padding: EdgeInsets.only(top: 70)),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: <Widget>[
              Container(
                width: 100,
                child: IconButton(
                  icon: Icon(
                    Icons.rotate_left,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => History()),
                    );
                  },
                ),
              ),
              Text("ประวัติการใช้ระบบ"),
            ],
          ),
        ],
      ),
    ]);
  }
}
