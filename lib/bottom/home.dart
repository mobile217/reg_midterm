import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:flutter/material.dart';
import '../home/guide.dart';
import '../menu/check_end.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: <Widget>[
          Text(
            "ข่าวสาร:",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          ImageSlideshow(
            width: double.infinity,
            height: 500,
            children: <Widget>[
              Container(
                child: Image.asset('images/img1.png'),
              ),
              Container(
                child: Image.asset('images/img2.png'),
              ),
              Container(
                child: Image.asset('images/img3.png'),
              ),
              Container(
                child: Image.asset('images/img4.png'),
              ),
              Container(
                child: Image.asset('images/img5.png'),
              ),
              Container(
                child: Image.asset('images/img6.png'),
              ),
              Container(
                child: Image.asset('images/img7.png'),
              ),
              Container(
                child: Image.asset('images/img8.png'),
              ),
              Container(
                child: Image.asset('images/img9.png'),
              ),
              Container(
                child: Image.asset('images/img10.png'),
              ),
            ],
            onPageChanged: (value) {
              print('Page changed: $value');
            },
            autoPlayInterval: 5000,
            isLoop: true,
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            "ประกาศเรื่อง:",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565",
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10,color: Colors.blue[900],decoration: TextDecoration.underline),
                    ),
                    Text(
                      " (ด่วน)",
                      style: TextStyle(fontWeight: FontWeight.bold,color: Colors.red, fontSize: 10),
                    ),
                  ],
                ),
                Image.asset('images/p1.png'),
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "**นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลา",
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.red,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "หากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯ จะไม่เสนอชื่อสำเร็จการศึกษา",
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.red,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                    Row(
                      children: [
                        InkWell(
                          child: Text("คู่มือการยื่นคำร้องสำเร็จการศึกษา",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline)),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Guide()),
                            );
                          },
                        ),
                        Text(
                            " นิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบและขึ้นทะเบียนนิสิต",
                            style: TextStyle(
                              fontSize: 10,
                              color: Colors.black,
                            ))
                      ],
                    ),
                    Column(children: [
                      Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "นิสิตสิตจะรับเอกสารจบการศึกษาได้ต่อเมื่อนิสิตมีสถานะจบการศึกษา",
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.black,),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Row(children: [
                        Text(
                        "โดยตรวจสอบที่",
                        style: TextStyle(
                            fontSize: 10,
                            color: Colors.black,),
                      ),
                      InkWell(
                          child: Text("เมนูตรวจสอบวันที่สำเร็จการศึกษา",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline)),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Check_End()),
                            );
                          },
                        ),
                      ],)
                    ),
                    ],)
                  ],
                )
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            child: Column(
              children: [
                Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                      "2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10,color: Colors.blue[900],decoration: TextDecoration.underline),
                    ),
                    ),
                    Image.asset('images/p2.png'),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            child: Column(
              children: [
                Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                      "3.LINE Official",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10,color: Colors.blue[900],decoration: TextDecoration.underline),
                    ),
                    ),
                    Image.asset('images/p5.png'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
