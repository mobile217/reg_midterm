import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:weather_app/routes/page.dart';
import 'Inmain.dart';
import 'login.dart';
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Reg BUU',
        // theme: ThemeData(
        //   primarySwatch: Color(0xFFCC0000),
        // ),
        home: 
        Login(),
        // Inmain(),
        routes: {
          page.login:(context) => Login(),
          page.inmain:(context) => Inmain(),
        },
      ),
    );
  }
}