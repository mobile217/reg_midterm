import 'package:flutter/material.dart';
import 'Reset.dart';
import 'Inmain.dart';

class Login extends StatelessWidget {
  static const String routeName = '/login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //     title: Text("Login Reg BUU",style: TextStyle(fontSize: 24, color: Colors.black)),
      //     automaticallyImplyLeading: false,
      //     backgroundColor: Colors.amber,
      //   ),
      backgroundColor: Color.fromARGB(255, 17, 17, 31),
      body: ListView(
        padding: EdgeInsets.only(top: 50),
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: 250, minHeight: 100, maxWidth: 250, minWidth: 100),
            child: Container(
              child: Image.asset(
                "images/logo.png",
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 30)),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFieldUsername(),
              Padding(padding: EdgeInsets.only(top: 20)),
              TextFieldPassword(),
              Padding(padding: EdgeInsets.only(top: 20)),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green, fixedSize: Size(250, 50)),
                child: const Text('LOGIN',
                    style: TextStyle(color: Colors.white, fontSize: 18)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Inmain()),
                  );
                },
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              Column(children: [
                InkWell(
                  child: Text("Forgot Password ?",
                      style: TextStyle(color: Colors.white)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Reset()),
                    );
                  },
                ),
              ]),
            ],
          ),
        ],
      ),
    );
  }
}

Container TextFieldUsername() {
  return Container(
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 380,
        minHeight: 50,
        minWidth: 300,
      ),
      child: TextField(
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.amber),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.amber),
          ),
          border: const OutlineInputBorder(),
          labelStyle: new TextStyle(color: Colors.grey),
          labelText: 'Username',
        ),
      ));
}

Container TextFieldPassword() {
  return Container(
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 380,
        minHeight: 50,
        minWidth: 300,
      ),
      child: TextField(
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        obscureText: true,
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.amber),
          ),
          enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.amber)),
          border: const OutlineInputBorder(),
          labelStyle: new TextStyle(color: Colors.grey),
          labelText: 'Password',
        ),
      ));
}
