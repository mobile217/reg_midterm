import 'package:flutter/material.dart';

class Socail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("เชื่อมต่อบัญชีอื่นๆ",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {}, title: Text("Facebook"),leading: Icon(Icons.facebook,color: Colors.blue,),),
        ListTile(onTap: () {}, title: Text("Mail"),leading: Icon(Icons.mail_outline,color: Colors.orange,),),
      ],
    ),
    );
  }
}