import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("ลงทะเบียนรับข่าวสาร",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {}, title: Text("กีฬา"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("สุขภาพ"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("กิจกรรม"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("บันเทิง"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("การเรียน"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("โปรโมชั่น"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("ทุน"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("อาหาร"), trailing: Icon(Icons.chevron_right)),
      ],
    ),
    );
  }
}