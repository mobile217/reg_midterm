import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Safety extends StatefulWidget {
  @override
  _SafetyState createState() => _SafetyState();
}

class _SafetyState extends State {
  bool state1 = true;
  bool state2 = false;
  bool state3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("ความปลอดภัย",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {}, title: Text("เปลี่ยน PIN"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("รีเซต PIN"), trailing: Icon(Icons.chevron_right)),
        ListTile(onTap: () {}, title: Text("ลายนิ้วมือ/Face ID"),subtitle:Text("ตรวจสอบข้อมูลด้วย ลายนิ้วมือ และ Face ID"), trailing: CupertinoSwitch(value: state1,onChanged: (value) {state1 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("แสดงข้อมูลติดต่อ"),subtitle:Text("อีเมลและเบอร์โทร"), trailing: CupertinoSwitch(value: state2,onChanged: (value) {state2 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("ประวัติตำแหน่ง"),trailing: CupertinoSwitch(value: state3,onChanged: (value) {state3 = value; setState((() {}));},activeColor: Colors.amber,)),
      ],
    ),
    );
  }
}