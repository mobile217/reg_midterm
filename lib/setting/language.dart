import 'package:flutter/material.dart';

class Language extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("ภาษา",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {}, title: Text("ภาษาไทย"), trailing: Icon(Icons.check)),
        ListTile(onTap: () {}, title: Text("English")),
      ],
    ),
    );
  }
}