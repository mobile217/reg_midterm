import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Warn extends StatefulWidget {
  @override
  _WarnState createState() => _WarnState();
}

class _WarnState extends State {
  bool state1 = true;
  bool state2 = true;
  bool state3 = true;
  bool state4 = true;
  bool state5 = true;
  bool state6 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("การแจ้งเตือน",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ListTile(onTap: () {}, title: Text("ได้รับการแจ้งเตือน"),subtitle:Text("ปิดการแจ้งเตือนทั้งหมดชั่วคราว"), trailing: CupertinoSwitch(value: state1,onChanged: (value) {state1 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("ข่วนและกิจกรรมที่น่าสนใจ"),subtitle:Text("ข่าวและกิจกรรมที่ได้รับความสนใจ"), trailing: CupertinoSwitch(value: state2,onChanged: (value) {state2 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("แจ้งเตือนแอปพลิเคชัน"),subtitle:Text("แจ้งเตือนในปฏิทิน"), trailing: CupertinoSwitch(value: state3,onChanged: (value) {state3 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("กิจกรรมที่ติดตาม"),subtitle:Text("มีการปรับปรุงกิจกรรมที่ติดตาม"), trailing: CupertinoSwitch(value: state4,onChanged: (value) {state4 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("ข่าวล่าสุดของมหาวิทยาลัย"),subtitle:Text("ข่าวสารอย่างเป็นทางการในวิทยาเขต"), trailing: CupertinoSwitch(value: state5,onChanged: (value) {state5 = value; setState((() {}));},activeColor: Colors.amber,)),
        ListTile(onTap: () {}, title: Text("ข่าวล่าสุดของคณะ"),subtitle:Text("ข่าวสารจากคณะ"), trailing: CupertinoSwitch(value: state6,onChanged: (value) {state6 = value; setState((() {}));},activeColor: Colors.amber,)),
      ],
    ),
    );
  }
}