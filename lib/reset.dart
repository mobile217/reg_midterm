import 'package:flutter/material.dart';
import 'Login.dart';

class Reset extends StatelessWidget {
  static const String routeName = '/login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //     title: Text("Login Reg BUU",style: TextStyle(fontSize: 24, color: Colors.black)),
      //     automaticallyImplyLeading: false,
      //     backgroundColor: Colors.amber,
      //   ),
      backgroundColor: Color.fromARGB(255, 17, 17, 31),
      body: ListView(
        padding: EdgeInsets.only(top: 50),
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: 250, minHeight: 100, maxWidth: 250, minWidth: 100),
            child: Container(
              child: Image.asset(
                "images/logo.png",
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 30)),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFieldUsername(),
              Padding(padding: EdgeInsets.only(top: 20)),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue, fixedSize: Size(250, 50)),
                child: const Text('HELP ME',
                    style: TextStyle(color: Colors.white, fontSize: 18)),
                onPressed: () {},
              ),
              Padding(padding: EdgeInsets.only(top: 10)),
              Text("Enter your username or email. The system will send a message to your email address.",style: TextStyle(color: Colors.grey),textAlign: TextAlign.center,),
              Padding(padding: EdgeInsets.only(top: 10)),
              Column(children: [
                InkWell(
                  child: Text("GO BACK",style: TextStyle(color: Colors.blue)),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Login()),
                    );
                  },
                ),
              ]),
            ],
          ),
        ],
      ),
    );
  }
}

Container TextFieldUsername() {
  return Container(
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 380,
        minHeight: 50,
        minWidth: 300,
      ),
      child: TextField(
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.amber),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.amber),
          ),
          border: const OutlineInputBorder(),
          hintText:"Username or Email",
          hintStyle: TextStyle(color: Colors.grey)
        ),
      )
      );
}
