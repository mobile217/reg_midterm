import 'package:flutter/material.dart';

class Fri extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.keyboard_backspace,
              color: Colors.black,
            ),
          ),
          title: Text("Friday",
              style: TextStyle(fontSize: 22, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.blue[200],
        ),
        backgroundColor: Colors.black,
        body: ListView(
          children: <Widget>[
                Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("13:00 - 15:00",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Text("88634259-59 (Mult)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("ห้อง ", style: TextStyle(fontSize: 16)),
                        Text("IF-4C01 ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text("(Lab)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.red)),
                      ],
                    ),
                    Text("อาจารย์ ดร.อุรีรัฐ สุขสวัสดิ์ชน",
                        style: TextStyle(fontSize: 16)),
                    Text("อีเมล : ureerat@go.buu.ac.th",
                        style: TextStyle(fontSize: 16)),
                  ],
                )
                ),
                Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("15:00 - 17:00",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Text("88634459-59 (Moblie)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("ห้อง ", style: TextStyle(fontSize: 16)),
                        Text("IF-3C01 ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text("(Lab)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.red)),
                      ],
                    ),
                    Text("อาจารย์ ดร.จักริน สุขสวัสดิ์ชน",
                        style: TextStyle(fontSize: 16)),
                    Text("อีเมล : jakkarin@go.buu.ac.th",
                        style: TextStyle(fontSize: 16)),
                  ],
                )
                ),
          ],
        ));
  }
}
