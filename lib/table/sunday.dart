import 'package:flutter/material.dart';

class Sun extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.keyboard_backspace,
              color: Colors.black,
            ),
          ),
          title: Text("Sunday",
              style: TextStyle(fontSize: 22, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.red[200],
        ),
        backgroundColor: Colors.black,
        body: ListView(
          children: <Widget>[
            Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("ไม่มีการเรียนการสอน",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                  ],
                )
                ),
          ],
        ));
  }
}
