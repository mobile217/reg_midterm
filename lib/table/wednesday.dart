import 'package:flutter/material.dart';

class Wed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.keyboard_backspace,
              color: Colors.black,
            ),
          ),
          title: Text("Wednesday",
              style: TextStyle(fontSize: 22, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.green[200],
        ),
        backgroundColor: Colors.black,
        body: ListView(
          children: <Widget>[
            Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("10:00 - 11:00",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Text("88634459-59 (Mobile)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("ห้อง ", style: TextStyle(fontSize: 16)),
                        Text("IF-4C01 ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text("(Lec)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.green)),
                      ],
                    ),
                    Text("อาจารย์ ดร.จักริน สุขสวัสดิ์ชน",
                        style: TextStyle(fontSize: 16)),
                    Text("อีเมล : jakkarin@go.buu.ac.th",
                        style: TextStyle(fontSize: 16)),
                  ],
                )
                ),
                Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("13:00 - 16:00",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Text("88646259-59 (Intro)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("ห้อง ", style: TextStyle(fontSize: 16)),
                        Text("IF-6T02 ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text("(Lec)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.green)),
                      ],
                    ),
                    Text("อาจารย์วรัณรัชญ์ วิริยะวิทย์",
                        style: TextStyle(fontSize: 16)),
                    Text("อีเมล : nlp.lec.buu@gmail.com",
                        style: TextStyle(fontSize: 16)),
                  ],
                )
                ),
                Container(
                margin: EdgeInsets.all(10),
                height: 150,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(30),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("17:00 - 19:00",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    Text("88624359-59 (Web)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("ห้อง ", style: TextStyle(fontSize: 16)),
                        Text("IF-3C01 ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold)),
                        Text("(Lab)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.red)),
                      ],
                    ),
                    Text("อาจารย์วรวิทย์ วีระพันธุ์",
                        style: TextStyle(fontSize: 16)),
                    Text("อีเมล : werapan@go.buu.ac.th",
                        style: TextStyle(fontSize: 16)),
                  ],
                )
                ),
          ],
        ));
  }
}
