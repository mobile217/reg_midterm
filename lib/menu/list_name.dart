import 'package:flutter/material.dart';

class List_Name extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("รายชื่อนิสิต",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        Text(
            "รายชื่อนิสิต",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Container(
            child: Image.asset("images/name.png"),
          ),
      ],
    ),
    );
  }
}