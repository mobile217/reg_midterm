import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class Request extends StatefulWidget {
  const Request({Key? key}) : super(key: key);

  @override
  State<Request> createState() => _Request();
}

class _Request extends State<Request> {
  final List<String> items = [
  ];
  String? selectedValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: Colors.black,
          ),
        ),
        title: Text("ยื่นคำร้อง",
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Text(
            "คำร้อง",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Text("อังคาร, 27 มกราคม 2023"),
          Row(
            children: [
              Text(
                "ขั้นที่ 1 เลือก ชนิดคำขอ",
              ),
              Padding(padding: EdgeInsets.only(left: 5)),
              DropdownButtonHideUnderline(
                child: DropdownButton2(
                  hint: Text(
                    '',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).hintColor,
                    ),
                  ),
                  items: items
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ))
                      .toList(),
                  value: selectedValue,
                  onChanged: (value) {
                    setState(() {
                      selectedValue = value as String;
                    });
                  },
                ),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(children: [
            Text("ขั้นที่ 2 "),
            Text("ระบุปีการศึกษา "),
            IconButton(icon: Icon(Icons.keyboard_arrow_left),onPressed: (){},color: Colors.blue,),
            Text("2565",style: TextStyle(color: Colors.purple),),
            IconButton(icon: Icon(Icons.keyboard_arrow_right),onPressed: (){},color: Colors.blue,),
            Text("/ ",style: TextStyle(color: Colors.purple),),
            Text("1 ",style: TextStyle(color: Colors.blue),),
            Text("2 ",style: TextStyle(color: Colors.purple),),
            Text("ฤดูร้อน",style: TextStyle(color: Colors.blue),),
          ],),
          Text("ขั้นที่ 3 กรอกข้อความคำร้องลงในช่องว่าง (ไม่เกิน 100 ตัวอักษร)"),
          Padding(padding: EdgeInsets.only(top: 10)),
          TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
  ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(children: [
            Text("จำนวนตัวอักษร "),
            Text("    0     "),
          ElevatedButton(
            style: ElevatedButton.styleFrom(backgroundColor: Colors.grey),
            child: const Text('นับจำนวนตัวอักษร',
                style: TextStyle(color: Colors.black)),
            onPressed: () {},
          ),
          ],),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(children: [
            Text("ขั้นที่ 4 กดปุ่ม  "),
          ElevatedButton(
            style: ElevatedButton.styleFrom(backgroundColor: Colors.grey),
            child: const Text('บันทึก',
                style: TextStyle(color: Colors.black)),
            onPressed: () {},
          ),
          ],),
        ],
      ),
    );
  }
}
