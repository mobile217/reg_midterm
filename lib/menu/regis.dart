import 'package:flutter/material.dart';

class Regis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: Colors.black,
          ),
        ),
        title: Text("ลงทะเบียนเรียน",
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        padding: const EdgeInsets.all(10),
        children: <Widget>[
          Text(
            "รายวิชาที่ต้องการลงทะเบียน",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(
            children: [
              Text(
                "ภาคการศึกษาที่ 2/2566",
              ),
              Text(
                "       เลือกหน้าจอบันทึกแบบ",
              ),
              Text(
                " ปกติ",
              ),
              Text(
                " พิเศษ",
                style: TextStyle(color: Colors.blue),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Table(
            border:
                TableBorder.all(color: Colors.black, style: BorderStyle.solid),
            children: [
              TableRow(
                children: [
                  Column(
                    children: [
                      Text(
                        "รหัสวิชา",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "คำอธิบาย",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "หน่วยกิต",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "กลุ่ม",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ],
              ),
              TableRow(
                children: [
                  Column(
                    children: [Text("")],
                  ),
                  Column(
                    children: [Text("")],
                  ),
                  Column(
                    children: [Text("")],
                  ),
                  Column(
                    children: [Text("")],
                  ),
                ],
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          ElevatedButton(
            style: ElevatedButton.styleFrom(backgroundColor: Colors.orange),
            child: const Text('ยืนยันการลงทะเบียน',
                style: TextStyle(color: Colors.white, fontSize: 18)),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
