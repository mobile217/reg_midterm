import 'package:flutter/material.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading:GestureDetector(
            onTap: () {Navigator.of(context).pop();},
            child: Icon(Icons.keyboard_backspace,color: Colors.black,),
          ),
          title: Text("ประวัติการใช้ระบบ",style: TextStyle(fontSize: 24, color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.amber,
        ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        Text(
            "สถิติการเข้าใช้ระบบ",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Row(children: [
            Text("ประจำเดือน "),
            IconButton(icon: Icon(Icons.keyboard_arrow_left),onPressed: (){},color: Colors.blue,),
            Text("มกราคม 2565",style: TextStyle(color: Colors.purple),),
            IconButton(icon: Icon(Icons.keyboard_arrow_right),onPressed: (){},color: Colors.blue,),
          ],),
          Table(border:
                TableBorder.all(color: Colors.black, style: BorderStyle.solid),
                children: [
            TableRow(children: [
              Column(children: [
                Text("")
              ]),
              Column(children: [
                Text("")
              ]),
              Column(children: [
                Text("เวลา")
              ]),
              Column(children: [
                Text("หมายเหตุ")
              ]),
              Column(children: [
                Text("จาก(IP)")
              ]),
              Column(children: [
                Text("ตรวจสอบข้อมูลโดย")
              ]),
            ]),
            TableRow(children: [
              Column(children: [
                Text("1")
              ]),
              Column(children: [
                Text("6")
              ]),
              Column(children: [
                Text("12.12")
              ]),
              Column(children: [
                Text("เข้าสู่ระบบสำเร็จ")
              ]),
              Column(children: [
                Text("999.256.48.15")
              ]),
              Column(children: [
                Text("")
              ]),
            ]),
            TableRow(children: [
              Column(children: [
                Text("2")
              ]),
              Column(children: [
                Text("7")
              ]),
              Column(children: [
                Text("22.36")
              ]),
              Column(children: [
                Text("เข้าสู่ระบบสำเร็จ")
              ]),
              Column(children: [
                Text("999.256.48.15")
              ]),
              Column(children: [
                Text("")
              ]),
            ]),
            TableRow(children: [
              Column(children: [
                Text("3")
              ]),
              Column(children: [
                Text("")
              ]),
              Column(children: [
                Text("15.25")
              ]),
              Column(children: [
                Text("เข้าสู่ระบบสำเร็จ")
              ]),
              Column(children: [
                Text("999.256.48.15")
              ]),
              Column(children: [
                Text("")
              ]),
            ]),
          ],),
      ],
    ),
    );
  }
}