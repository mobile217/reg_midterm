import 'package:flutter/material.dart';

class Results extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: Colors.black,
          ),
        ),
        title: Text("ผลการศึกษา",
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Text(
            "ผลการศึกษา",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            child: Image.asset("images/grade.png"),
          ),
        ],
      ),
    );
  }
}
