import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class Check_End extends StatefulWidget {
  const Check_End({Key? key}) : super(key: key);

  @override
  State<Check_End> createState() => _End();
}

class _End extends State<Check_End> {
  final List<String> items = [
    'แสดงข้อมูลสรุป',
    'แสดงรายละเอียดแบบที่ 1 ทั้งหลักสูตร',
    'แสดงรายละเอียดแบบที่ 1 เฉพาะรายวิชาที่ลง',
  ];
  String? selectedValue = 'แสดงข้อมูลสรุป';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: Colors.black,
          ),
        ),
        title: Text("ตรวจสอบจบ",
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Text(
            "ตรวจสอบจบ",
            style: TextStyle(fontSize: 20, color: Colors.orange),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(
            children: [
              Text(
                "โปรดเลือก",
                style: TextStyle(fontSize: 16, color: Colors.purple),
              ),
              Padding(padding: EdgeInsets.only(left: 5)),
              DropdownButtonHideUnderline(
                child: DropdownButton2(
                  hint: Text(
                    'Select Item',
                    style: TextStyle(
                      fontSize: 14,
                      color: Theme.of(context).hintColor,
                    ),
                  ),
                  items: items
                      .map((item) => DropdownMenuItem<String>(
                            value: item,
                            child: Text(
                              item,
                              style: const TextStyle(
                                fontSize: 14,
                              ),
                            ),
                          ))
                      .toList(),
                  value: selectedValue,
                  onChanged: (value) {
                    setState(() {
                      selectedValue = value as String;
                    });
                  },
                ),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            child: Image.asset("images/end.png"),
          ),
        ],
      ),
    );
  }
}
