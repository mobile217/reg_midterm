import 'package:flutter/material.dart';
import '../table/monday.dart';
import '../table/tuesday.dart';
import '../table/wednesday.dart';
import '../table/thursday.dart';
import '../table/friday.dart';
import '../table/saturday.dart';
import '../table/sunday.dart';


class TableS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.keyboard_backspace,
            color: Colors.black,
          ),
        ),
        title: Text("ตารางเรียน",
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
      ),
      body: ListView(
        padding: const EdgeInsets.only(top: 10),
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 40,
            child: const Center(
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'ค้นหาตารางเรียน',
                  prefixIcon: Icon(Icons.search),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
          ),
          ListTile(
              tileColor: Colors.yellow[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Mon()),
                            );},
              title: Text("Monday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.pink[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Tue()),
                            );},
              title: Text("Tuesday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.green[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Wed()),
                            );},
              title: Text("Wednesday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.orange[200],
              onTap: () {
                Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Thurs()),
                            );
              },
              title: Text("Thursday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.blue[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Fri()),
                            );},
              title: Text("Friday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.purple[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Sat()),
                            );},
              title: Text("Saturday"),
              trailing: Icon(Icons.chevron_right)),
          Padding(
            padding: const EdgeInsets.only(top: 5),
          ),
          ListTile(
              tileColor: Colors.red[200],
              onTap: () {Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Sun()),
                            );},
              title: Text("Sunday"),
              trailing: Icon(Icons.chevron_right)),
        ],
      ),
    );
  }
}
