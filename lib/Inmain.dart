import 'package:flutter/material.dart';
import 'bottom/home.dart';
import 'bottom/menu.dart';
import 'bottom/profile.dart';
import 'bottom/setting.dart';

// enum APP_THEME { LIGHT, DARK }

// class MyAppTheme {
//   static ThemeData appThemeLight() {
//     return ThemeData(
//         brightness: Brightness.light,
//         appBarTheme: AppBarTheme(
//           color: Colors.amber,
//           // iconTheme: IconThemeData(
//           //   color: Colors.black,
//           // )
//         ),
//         iconTheme: IconThemeData(color: Colors.black),
//         bottomNavigationBarTheme: BottomNavigationBarThemeData(
//             selectedItemColor: Colors.amber,
//             unselectedItemColor: Colors.black));
//   }

//   static ThemeData appThemeDark() {
//     return ThemeData(
//         brightness: Brightness.dark,
//         appBarTheme: AppBarTheme(
//           color: Colors.amber,
//           // iconTheme: IconThemeData(
//           //   color: Colors.white,
//           // )
//         ),
//         iconTheme: IconThemeData(color: Colors.white),
//         bottomNavigationBarTheme: BottomNavigationBarThemeData(
//             selectedItemColor: Colors.amber,
//             unselectedItemColor: Colors.white));
//   }
// }

class Inmain extends StatefulWidget {
  static const String routeName = '/inmain';
  @override
  ExampleApp createState() => ExampleApp();
}

class ExampleApp extends State {
  // var currentTheme = APP_THEME.LIGHT;
  int _selectedIndex = 0;
  static const TextStyle optionStyle = TextStyle(fontSize: 30);
  List _widgetOptions = [
    Home(),
    Menu(),
    Profile(),
    Setting(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: const Text('มหาวิทยาลัยบูรพา',
            style: TextStyle(fontSize: 24, color: Colors.black)),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.amber,
        // actions: <Widget>[
        //   IconButton(
        //       icon: Icon(Icons.color_lens),
        //       color: Colors.black,
        //       onPressed: () {
        //         setState(() {
        //           currentTheme == APP_THEME.DARK
        //               ? currentTheme = APP_THEME.LIGHT
        //               : currentTheme = APP_THEME.DARK;
        //         });
        //       }),
        // ],
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.widgets),
            label: 'Menu',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Profile',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.black,
        selectedItemColor: Colors.amber,
        onTap: _onItemTapped,
      ),
    );
  }
}
